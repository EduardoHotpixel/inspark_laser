void oscEvent(OscMessage &m) { // *note the & before msg
  // receive a message
  //Serial.println("got a message");
  //in arduino, we plug events in the EVENT method
  m.plug("/enableSensor", myFunction);


}


void myFunction(OscMessage &m) {  // *note the & before msg
  // getting to the message data
  // getType takes position as a parameter
  float value = m.getFloat(0);
  if (value == 0) {
    initCuesStep = 0;
    stopCues();
    isSensing = false;
    Serial.println("is sensing? no");
  }


  if (value == 1) {
    initCuesStep = 1;
    switch (initCuesStep) {
      case 0:
        break;
      case 1:
        initCues();
        initCuesStep = 2;
        break;
    }
    Serial.println("is sensing? yes");
    isSensing = true;
  }

}
