#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <OscUDP.h>
//ETHERNET
byte mac[] = {0xDE, 0xA2, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress localip(10, 1, 1, 10  ); //LOCAL IP

//OSC
unsigned int listeningPort = 12001;// local port to listen on
NetAddress destination;
NetAddress destination2;

IPAddress destinationIP( 10, 1, 1, 9 );
IPAddress destinationIP2( 10, 1, 1, 100 );

int destinationPort = 8000;
// SETUP A UDP OBJET
EthernetUDP UDP;
// OUR OSC MESSAGE OBJECT
OscUDP etherOSC;
OscUDP etherOSC2;
int destinationPort2 = 9000;

//SENSOR
#define CTIMER1 5000//CUE TIMER 1//2.3
#define CTIMER2 5000//CUE TIMER 2//2.0
#define CTIMER3 5000//CUE TIMER 3//2.0
#define CTIMER4 5000//CUE TIMER 4//2.0

int pin1 = A1;//SENSOR PIN 1
int pin2 = A2;//SENSOR PIN 2
int pin3 = A3;//SENSOR PIN 3
int pin4 = A4;//SENSOR PIN 4

int sState1 = 0;// ESTADO INICIAL DEL SENSOR 1
int sState2 = 0;// ESTADO INICIAL DEL SENSOR 2
int sState3 = 0;// ESTADO INICIAL DEL SENSOR 3
int sState4 = 0;// ESTADO INICIAL DEL SENSOR 4

float sensor1Timer;//TEMPORIZADOR DE ESPERA SENSOR 1
float sensor2Timer;//TEMPORIZADOR DE ESPERA SENSOR 2
float sensor3Timer;//TEMPORIZADOR DE ESPERA SENSOR 3
float sensor4Timer;//TEMPORIZADOR DE ESPERA SENSOR 3

bool sActive1 = false;//IS SENSOR ACTIVE?
bool sActive2 = false;
bool sActive3 = false;
bool sActive4 = false;

bool isSensing = false;
bool bInitCues = false;
int initCuesStep = 0;
int stopCuesStep = 0;
void setup() {
  Serial.begin(115200);

  pinMode(pin1, INPUT);// SENSOR 1
  pinMode(pin2, INPUT);// SENSOR 2
  pinMode(pin3, INPUT);// SENSOR 3
  pinMode(pin4, INPUT);// SENSOR 4

  // Serial communications and wait for port to open:
  Ethernet.begin(mac, localip);
  Serial.print("Getting IP address...");
  Serial.print("IP address is ");
  Serial.println(Ethernet.localIP());

  //OSC
  UDP.begin(listeningPort);
  //set up our communication protocol
  //pass the UDP object to OSC
  etherOSC.begin(UDP);
  etherOSC2.begin(UDP);

  //define our destination location
  destination.set(destinationIP, destinationPort);
  destination2.set(destinationIP2, destinationPort2);


}

void loop() {

 if (isSensing == true) {
    sensor1();
    sensor2();
    sensor3();
    sensor4();
 }
  etherOSC.listen();
}
