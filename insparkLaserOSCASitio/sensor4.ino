
void sensor4() {
  sState4 = digitalRead(pin4);//SENSOR 4 DIGITAL PIN
  sensor4Timer = constrain(sensor4Timer, 0, CTIMER4);
  if ( sState4 == HIGH) {

    sActive4 = true;

    if (sensor4Timer >= 1 && sensor4Timer <= 1) {

      //STOP POINTER 4 CUE
      OscMessage stopPointer4("/beyond/general/StopCueNow");
      stopPointer4.add(0);
      stopPointer4.add(6);
      etherOSC.send(stopPointer4, destination);

      //START CUE 4
      OscMessage startCue4("/beyond/general/CueDown");
      startCue4.add(0);
      startCue4.add(7);
      etherOSC.send(startCue4, destination);


      //START CLIP 4
      OscMessage startClip4("/clip/");
      startClip4.add(8);
      etherOSC2.send(startClip4, destination2);

    }

  }


  if (sActive4 == true) {
    sensor4Timer++;
  }

  if (sensor4Timer >= CTIMER4) {

   //STOP CUE 4
    OscMessage stopCue4("/beyond/general/StopCueNow");
    stopCue4.add(0);
    stopCue4.add(7);
    etherOSC.send(stopCue4, destination);
    
    //START POINTER 4
    OscMessage startPointer4("/beyond/general/CueDown");
    startPointer4.add(0);
    startPointer4.add(6);
    etherOSC.send(startPointer4, destination);


 
    sActive4 = false;
    sensor4Timer = 0;

  }


}
