void stopCues() {
  //STOP POINTER 1 CUE
  OscMessage stopPointer1("/beyond/general/StopCueNow");
  stopPointer1.add(0);
  stopPointer1.add(8);
  etherOSC.send(stopPointer1, destination);

  //STOP CUE 1
  OscMessage stopCue1("/beyond/general/StopCueNow");
  stopCue1.add(0);
  stopCue1.add(9);
  etherOSC.send(stopCue1, destination);

  //STOP POINTER 2 CUE
  OscMessage stopPointer2("/beyond/general/StopCueNow");
  stopPointer2.add(0);
  stopPointer2.add(10);
  etherOSC.send(stopPointer2, destination);


  //STOP CUE 2
  OscMessage stopCue2("/beyond/general/StopCueNow");
  stopCue2.add(0);
  stopCue2.add(11);
  etherOSC.send(stopCue2, destination);

  //STOP POINTER 3 CUE
  OscMessage stopPointer3("/beyond/general/StopCueNow");
  stopPointer3.add(0);
  stopPointer3.add(12);
  etherOSC.send(stopPointer3, destination);

  //STOP CUE 3
  OscMessage stopCue3("/beyond/general/StopCueNow");
  stopCue3.add(0);
  stopCue3.add(13);
  etherOSC.send(stopCue3, destination);


  //STOP POINTER 4 CUE
  OscMessage stopPointer4("/beyond/general/StopCueNow");
  stopPointer4.add(0);
  stopPointer4.add(14);
  etherOSC.send(stopPointer4, destination);

  //STOP CUE 4
  OscMessage stopCue4("/beyond/general/StopCueNow");
  stopCue4.add(0);
  stopCue4.add(15);
  etherOSC.send(stopCue4, destination);

}
