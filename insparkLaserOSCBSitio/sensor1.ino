
void sensor1() {

  sState1 = digitalRead(pin1);//SENSOR 1 DIGITAL PIN
  sensor1Timer = constrain(sensor1Timer, 0, CTIMER1);

  if ( sState1 == HIGH) {
    sActive1 = true;

    if (sensor1Timer >= 1 && sensor1Timer <= 1) {

      //STOP POINTER 1 CUE
      OscMessage stopPointer1("/beyond/general/StopCueNow");
      stopPointer1.add(0);
      stopPointer1.add(8);
      etherOSC.send(stopPointer1, destination);

      //START CUE 1
      OscMessage startCue1("/beyond/general/CueDown");
      startCue1.add(0);
      startCue1.add(9);
      etherOSC.send(startCue1, destination);

      //START CLIP 1
      OscMessage startClip1("/clip/");
      startClip1.add(1);
      etherOSC2.send(startClip1, destination2);


    }

  }
  if (sActive1 == true) {
    sensor1Timer++;
  }

  if (sensor1Timer >= CTIMER1) {

    //STOP CUE 1
    OscMessage stopCue1("/beyond/general/StopCueNow");
    stopCue1.add(0);
    stopCue1.add(9);
    etherOSC.send(stopCue1, destination);


    //START POINTER 1
    OscMessage startPointer1("/beyond/general/CueDown");
    startPointer1.add(0);
    startPointer1.add(8);
    etherOSC.send(startPointer1, destination);




    sActive1 = false;
    sensor1Timer = 0;

  }


}
