
void sensor2() {

  sState2 = digitalRead(pin2);//SENSOR 2 DIGITAL PIN
  sensor2Timer = constrain(sensor2Timer, 0, CTIMER2);


  if ( sState2 == HIGH) {
    sActive2 = true;

    if (sensor2Timer >= 1 && sensor2Timer <= 1) {

      //STOP POINTER 2 CUE
      OscMessage stopPointer2("/beyond/general/StopCueNow");
      stopPointer2.add(0);
      stopPointer2.add(10);
      etherOSC.send(stopPointer2, destination);

      //START CUE 1
      OscMessage startCue2("/beyond/general/CueDown");
      startCue2.add(0);
      startCue2.add(11);
      etherOSC.send(startCue2, destination);

      //START CLIP 2
      OscMessage startClip2("/clip/");
      startClip2.add(2);
      etherOSC2.send(startClip2, destination2);

    }

  }


  if (sActive2 == true) {
    sensor2Timer++;
  }

  if (sensor2Timer >= CTIMER2) {

    //STOP CUE 2
    OscMessage stopCue2("/beyond/general/StopCueNow");
    stopCue2.add(0);
    stopCue2.add(11);
    etherOSC.send(stopCue2, destination);

    //START POINTER 2
    OscMessage startPointer2("/beyond/general/CueDown");
    startPointer2.add(0);
    startPointer2.add(10);
    etherOSC.send(startPointer2, destination);



    sActive2 = false;
    sensor2Timer = 0;
  }


}
