
void sensor3() {
  sState3 = digitalRead(pin3);//SENSOR 3 DIGITAL PIN
  sensor3Timer = constrain(sensor3Timer, 0, CTIMER3);
  if ( sState3 == HIGH) {

    sActive3 = true;

    if (sensor3Timer >= 1 && sensor3Timer <= 1) {

      //STOP POINTER 3 CUE
      OscMessage stopPointer3("/beyond/general/StopCueNow");
      stopPointer3.add(0);
      stopPointer3.add(12);
      etherOSC.send(stopPointer3, destination);

      //START CUE 3
      OscMessage startCue3("/beyond/general/CueDown");
      startCue3.add(0);
      startCue3.add(13);
      etherOSC.send(startCue3, destination);


      //START CLIP 3
      OscMessage startClip3("/clip/");
      startClip3.add(3);
      etherOSC2.send(startClip3, destination2);


    }

  }


  if (sActive3 == true) {
    sensor3Timer++;
  }

  if (sensor3Timer >= CTIMER3) {


    //STOP CUE 3
    OscMessage stopCue3("/beyond/general/StopCueNow");
    stopCue3.add(0);
    stopCue3.add(13);
    etherOSC.send(stopCue3, destination);


    //START POINTER 3
    OscMessage startPointer3("/beyond/general/CueDown");
    startPointer3.add(0);
    startPointer3.add(12);
    etherOSC.send(startPointer3, destination);

    sActive3 = false;
    sensor3Timer = 0;



  }


}
