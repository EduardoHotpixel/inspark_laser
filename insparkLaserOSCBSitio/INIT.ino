void initCues() {

  //SET TRANSITION OFF
  OscMessage transition("/beyond/general/MasterTransition");
  transition.add(0);
  etherOSC.send(transition, destination);
  
  //START POINTER 1
  OscMessage startPointer1("/beyond/general/CueDown");
  startPointer1.add(0);
  startPointer1.add(8);
  etherOSC.send(startPointer1, destination);


  //STOP CUE 1
  OscMessage stopCue1("/beyond/general/StopCueNow");
  stopCue1.add(0);
  stopCue1.add(9);
  etherOSC.send(stopCue1, destination);

  //START POINTER 2
  OscMessage startPointer2("/beyond/general/CueDown");
  startPointer2.add(0);
  startPointer2.add(10);
  etherOSC.send(startPointer2, destination);


  //STOP CUE 2
  OscMessage stopCue2("/beyond/general/StopCueNow");
  stopCue2.add(0);
  stopCue2.add(11);
  etherOSC.send(stopCue2, destination);

  //START POINTER 3
  OscMessage startPointer3("/beyond/general/CueDown");
  startPointer3.add(0);
  startPointer3.add(12);
  etherOSC.send(startPointer3, destination);


  //STOP CUE 3
  OscMessage stopCue3("/beyond/general/StopCueNow");
  stopCue3.add(0);
  stopCue3.add(13);
  etherOSC.send(stopCue3, destination);

  //START POINTER 4
  OscMessage startPointer4("/beyond/general/CueDown");
  startPointer4.add(0);
  startPointer4.add(14);
  etherOSC.send(startPointer4, destination);


  //STOP CUE 4
  OscMessage stopCue4("/beyond/general/StopCueNow");
  stopCue4.add(0);
  stopCue4.add(15);
  etherOSC.send(stopCue4, destination);


}
